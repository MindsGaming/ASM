<div>
	<div align="center"><img height="200px" width="375px" src="./0.png"></div>
	<br>
	<div align="center">*see MP.ino for UF Stack implementation</div>
	<h1>🌱Greenhouse MEAN Stack</h1>
	<div align="center">
		<img height="100px" width="100px" src="./morningstar.png">
		<img height="100px" width="100px" src="./arduino.jpg">
		<img height="100px" width="100px" src="./java_logo.png">
		<img height="100px" width="100px" src="./Python_Logo.png">
		<img height="100px" width="100px" src="./html_css_js.png">
		<img height="100px" width="100px" src="./node_logo.png">
		<img height="100px" width="100px" src="./angularjs_logo.png">
	</div>
	<div>
	A Full Software Stack to gather, interact with, and record data from an environmental energy sensing, storing, and harvesting Hardware Stack.
	</div>
	<div>
		<h4>FrontEnd</h4>
		<h5>Client Side</h5>
		<ul>Uses AngularJs, CSS and/or animation styles to visually display vitals and sig events</ul>
		<p><h5>SmartHome Dashboard:</h5>
			<ul>Displays all controllers with links to their homepages</ul>
			<ul>Displays Power data from MorningStar and keeps historical database</ul>
			<ul>Displays status of water lines from Arduino Yun garden program</ul>
			<ul>Allows for control of certain actuators via web UI</ul>
			<ul>Displays all useful incoming Bridge data points real-time</ul>
			<ul>Has navigation buttons to all live “rooms” under control 
				(i.e., Brewery, Shroomery, Greenhouse, ChickenCoop, Surveillance)</ul>
			<ul>Ledger – track income and expenses</ul>
			<ul>Inventory Database</ul>
			<ul>Displays historical charts</ul>
			<ul>Weather Station (compares forecast to actual experience)</ul>
			<ul>Links to Bankless Store Front with crowdfunding capability</ul>
		</p>
		<h4>BackEnd</h4>
		<p><h5>Java:</h5>
			<ul>Environment Sensing</ul>
			<ul>Power Management (uses incoming MorningStar data to run a power management program)</ul>
			<ul>GreenHouse/Garden Control (controls water, artificial light, airflow, humidty, etc.)</ul>
			<ul>Task Timing/Control (what happens when how often)</ul>
			<ul>Datalog</ul>
			<ul>Python Calls</ul>
			<ul>Low Voltage Actualization/Realization</ul>
		</p>
		<p><h5>Python:</h5>
			<ul>Link to MorningStar via Modbus</ul>
			<ul>Log data to file</ul>
			<ul>Get Weather</ul>
			<ul>Send Sig Event Emails</ul>
			<ul>Filesystem functions</ul>
		</p>
		<p><h4>The Hardware:</h4>
			<ul>Arduino micro controller coupled with a solar circuit using
				a Morningstar solar charge controller complete with panels
				and battery bank.
			</ul>
		</p>
	</div>

<div>
**Though the microcontrollers hold significant amounts of data and are stand alone units running SmartHome© and GreenHouse© , they are ultimately meant to connect to a localhost running on a more sophisticated processor where StoreFront©, BehindTheCounter©, and AngelTokens© can be implemented for Sales Inventory and Payflow as well as Crowdfunding.**
</div>

</div>  
<a src="https://lucid.app/lucidchart/2583a382-273a-48f7-9c58-65a5fc4d5cf9/edit?view_items=qJbpXF30HLLe&invitationId=inv_0d67ca74-1058-44e4-b4be-61e5acd26da4#">Lucid Chart</a>
<hr/>
<div align="center"><img height="200px" width="375px" src="./UnifiedField.png"></div>
<div>
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/134360883-0160120a-41fe-4a07-850f-14e03be9f175.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/134360691-9bb2c9de-e3cb-4803-990f-5c536fb11b21.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/134360847-09d73560-56ec-49de-848f-7ab9ce15bcd8.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/119382357-eb5cba00-bc8f-11eb-8f67-6b60b4703688.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/134360854-8baa8306-39c4-4c5b-ab7b-5a7e5d0871df.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/134360864-7627cc5d-9033-410a-a4c7-f878eb1a0bc8.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/175050338-9bba38e4-9c23-410e-b238-0191f300e7ea.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/175050507-791ff907-8ce6-4aad-8843-2f4bf41c96eb.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/175050548-5557915e-9bbe-466d-9c19-ee33a066d210.png">
	<img height="250px" width="250px" src="https://user-images.githubusercontent.com/75486638/175050560-adb8f392-b930-4f36-96e2-12347286823e.png">
</div>
